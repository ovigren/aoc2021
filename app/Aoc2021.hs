module Main where
import System.Environment
import qualified D1.SonarSweep as D1
import qualified D2.Dive as D2
import qualified D3.BinaryDiagnostic as D3
import qualified D4.GiantSquid as D4
import qualified D5.HydrothermalVenture as D5
import qualified D6.Lanternfish as D6
import qualified D7.TheTreacheryofWhales as D7
import qualified D8.SevenSegmentSearch as D8
import qualified D9.SmokeBasin as D9
import qualified D10.SyntaxScoring as D10
import qualified D11.DumboOctopus as D11
import qualified D12.PassagePathing as D12
import qualified D13.TransparentOrigami as D13
import qualified D14.ExtendedPolymerization as D14
import qualified D15.Chiton as D15
import qualified D16.PacketDecoder as D16
-- IMPORT TARGET

main = getArgs >>= \(day : chosenInput) ->
  exec day (findInput day chosenInput)

findInput :: String -> [String] -> String
findInput day input = "input/" ++ day ++ "/" ++ case input of
  [] -> "input"
  [s] -> s

exec = \case
  "1" -> D1.main
  "2" -> D2.main
  "3" -> D3.main
  "4" -> D4.main
  "5" -> D5.main
  "6" -> D6.main
  "7" -> D7.main
  "8" -> D8.main
  "9" -> D9.main
  "10" -> D10.main
  "11" -> D11.main
  "12" -> D12.main
  "13" -> D13.main
  "14" -> D14.main
  "15" -> D15.main
  "16" -> D16.main
  -- CASE TARGET
  _    -> const $ putStrLn "No such day"
