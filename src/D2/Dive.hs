module D2.Dive where
import MyPrelude
import Data.Char
import Control.Arrow

main path = do
  str <- map (second (read @Int) . break isSpace)
         . lines
         <$> readFile path
  print $ partOne str
  print $ partTwo str

partOne :: [(String, Int)] -> Int
partOne = uncurry (*) . dmap sum . unzip . map
          (\case             -- hz, dp
              ("forward", x) -> (x, 0)
              ("down",    x) -> (0, x)
              ("up",      x) -> (0, -x)
          )

partTwo :: [(String, Int)] -> Int
partTwo = (\(hz, dp, _) -> hz * dp)
          . flip foldl (0, 0, 0) (\(hz, dp, aim) -> \case
              ("down",    x) -> (hz,     dp,           aim + x) ;
              ("up",      x) -> (hz,     dp,           aim - x) ;
              ("forward", x) -> (hz + x, dp + aim * x, aim))
