module D7.TheTreacheryofWhales where
import MyPrelude
import Data.List

main path = do
   input <- sort . map (read @Int) . splitOn ","
            <$> readFile path
   print $ partOne input
   print $ partTwo input

partOne :: [Int] -> Int
partOne crabs =
  sum $ flip map crabs $ \c -> abs $ c - median
  where median = head $ drop (length crabs `div` 2) $ crabs


partTwo :: [Int] -> Int
partTwo crabs = minimum exp `div` 2
    where
      mean = fromIntegral (sum crabs) / fromIntegral (length crabs)
      optimums = [ floor (mean - 0.5) .. ceiling (mean + 0.5) ]

      exp = [   sum [ (c - x)^2   | c <- crabs]
              + sum [ abs (c - x) | c <- crabs]
            | x <- optimums]

      -- ...
      -- ↑ == ↓

      exp1 = [ length crabs * x^2
               + sum (map (^2) crabs)
               - 2*x * sum crabs
               + sum [ signum(c-x) * (c - x) | c <- crabs]
             | x <- optimums]

      exp2 = [ length crabs * x^2
                + sum (map (^2) crabs)
                - 2*x * sum crabs
                + let (leftCrabs, mrightCrabs) = span (x >) crabs in
                 sum [ -1 * (c - x) | c <- leftCrabs]
                 + sum [1 * (c - x) | c <- mrightCrabs]
              | x <- optimums]

      exp3 = [ length crabs * x^2
                  + sum (map (^2) crabs)
                  + let (leftCrabs, mrightCrabs) = span (x >) crabs in
                      x * (length leftCrabs - length mrightCrabs)
                      - 2*x * sum crabs
                      + sum mrightCrabs - sum leftCrabs
               | x <- optimums]

      exp4 = [ let (leftCrabs, mrightCrabs) = span (x >) crabs in
                  sum (map (^2) crabs)
                  + x * (length crabs * x + length leftCrabs - length mrightCrabs)
                  - 2*x * sum crabs + sum mrightCrabs - sum leftCrabs
               | x <- optimums]

      exp5 = [ let (leftCrabs, mrightCrabs) = span (x >) crabs in
                  sum (map (^2) crabs)
                  + x * (length crabs * (x-1) + 2 * length leftCrabs)
                  - 2 * (x * sum crabs + sum leftCrabs)
                  + sum crabs
               | x <- optimums]

      toto = (sum (map (^2) crabs) + sum crabs + minimum expC) `div` 2
        where
          expC = [ let (leftCrabs, mrightCrabs) = span (x >) crabs in
                      x * (length crabs * (x-1) + 2 * length leftCrabs)
                      - 2 * (x * sum crabs + sum leftCrabs)
                  | x <- optimums]

-- Choose x such that minify
  -- y = ∑_c∈crabs. ∑_k∈[0..abs(x-c)]. k
  -- y = ∑_c∈crabs. 1/2 (abs(x-c))(abs(x-c)+1)
  -- y = 1/2 ∑_c∈crabs. c^2 - 2cx + abs(x-c) + x^2
  -- let l = len(crabs), sum = ∑_c∈crabs and s = sum(c)
  -- y = 1/2 (sum(c^2) - 2x*s + sum(abs((x-c))) + l*x^2)

  -- Differentiate,
  -- y' = 1/2 (- 2*s + sum((x-c)/abs(x-c)) + l*2x)
  -- y' = -s + sum((x-c)/abs(x-c))/2 + l*x

  -- 0 = -s + l*x + sum((x-c)/abs(x-c))/2
  -- Divide sum into crabs right of x, crabs left of x
  -- 0 = -s + l*x + ∑c₋∈crabs←(-1)/2 + ∑c₊∈crabs→(1)/2
  -- 0 = -s + l*x + -len(crabs←)/2 + len(crabs→)/2
  -- x = s/l + len(crabs←)/2l - len(crabs→)/2l
  -- x = s/l + (len(crabs←)/l2 - (l - len(crabs←))/l2
  -- x = s/l + (2len(crabs←) - l)/l2
  -- x = s/l + len(crabs←)/l - 1/2
  -- len(crabs←)/l - 1/2  is between -1/2 and 1/2
  -- s/l - 1/2  ≤  x  ≤  s/l + 1/2

  -- ex. If crabs are [0, 1] then best real position is
  -- 0.5, but integer is either 0 or 1.
