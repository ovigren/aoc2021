module D12.PassagePathing where
import MyPrelude
import qualified Data.Map.Strict as M
import Data.List
import Data.Char (isUpper)
import Data.Maybe (fromJust)
import Data.Tuple (swap)

main path = do
  input <- M.fromList
           . map (\x -> (fst $ head x, map snd x) ) -- }
           . groupBy (\a b -> fst a == fst b)       -- } (A-B),(A-C)
           . sortOn fst                             -- }   -> (A-[B, C])
           . concatMap (\x -> [x, swap x]) -- Make undirected
           . map (splitPairOn "-") . lines <$> readFile path
  print . length $ search input False ["start"]
  print . length $ search input True ["start"]

search :: M.Map String [String]
       -> Bool -- ^ Can still visit single lowercase cave twice
       -> [String]
       -> [[String]]
search m doubleLeft path@(current : _) = fromJust (M.lookup current m) >>= \case
    "end"                                 -> ["end" : path]
    "start"                               -> []
    x | all isUpper x || x `notElem` path -> search m doubleLeft (x : path)
      | doubleLeft                        -> search m False (x : path)
      | otherwise                         -> []
