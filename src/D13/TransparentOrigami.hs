module D13.TransparentOrigami where
import Data.List (nub)
import Data.Maybe (mapMaybe)
import Data.Tuple (swap)
import MyPrelude

type HCoords = [(Int, Int)] -- ^ x,y coordinates for hashes
type Instr = (Char, Int)

main path = do
  input <- bimap (map (dmap (read @Int) . splitPairOn ",") . lines)
                 (map (bimap last (read @Int) . splitPairOn "=") . lines)
           . splitPairOn "\n\n" <$> readFile path
           :: IO (HCoords, [Instr])

  print $ uncurry partOne input
  putStr $ uncurry partTwo input


partOne :: HCoords -> [Instr] -> Int
partOne m inst = length $ foldl fold m (take 1 inst)

partTwo :: HCoords -> [Instr] -> String
partTwo m inst = let finalCoords = foldl fold m inst in
  unlines $ [ [ if (x, y) `elem` finalCoords then '❄' else ' '
              | x <- [0 .. maximum (map fst finalCoords)]]
            | y <- [0 .. maximum (map snd finalCoords)]]

fold :: HCoords -> Instr -> HCoords
fold m ('x', n) = foldHorz n m
fold m ('y', n) = foldVert n m

foldHorz, foldVert :: Int -> HCoords -> HCoords
foldHorz n = map swap . foldVert n . map swap
foldVert n = nub . mapMaybe (\(x, y) -> case compare y n of
                                LT -> Just (x, y)       -- Upper part is static
                                EQ -> Nothing           -- Crease row is removed
                                GT -> Just (x, 2*n - y) -- Bottom part is folded
                            )
