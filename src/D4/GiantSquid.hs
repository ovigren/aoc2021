module D4.GiantSquid where
import Data.Maybe
import Data.List
import Data.List.Split

main path = do
  (kenoStr : boardsStr) <- splitOn "\n\n" <$> readFile path

  let keno = read @Int <$> splitOn "," kenoStr
  let boards = map (map (Just . read @Int) . words) . lines <$> boardsStr

  print $ partOne boards keno
  print $ partTwo boards keno


partOne :: [[[Maybe Int]]] -> [Int] -> Int
partOne boards (k:keno) =
  let marked = map (mark k) boards
  in case filter bingo marked of
       [] -> partOne marked keno
       [winner] -> score k winner

mark k = map $ map (>>= (\x -> if x == k
                               then Nothing
                               else Just x))

bingo :: [[Maybe Int]] -> Bool
bingo s =  any (all isNothing) s
        || any (all isNothing) (transpose s)

score k = (k *) . sum . catMaybes . concat

partTwo :: [[[Maybe Int]]] -> [Int] -> Int
partTwo [loser] (k:keno) =
  let loser' = mark k loser
  in if bingo loser'
     then score k loser'
     else partTwo [loser'] keno
partTwo boards (k:keno) =
  let marked = map (mark k) boards
  in partTwo (filter (not . bingo) marked) keno
