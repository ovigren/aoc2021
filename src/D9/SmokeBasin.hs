module D9.SmokeBasin where
import Control.Monad (join)
import Data.Foldable (toList)
import Data.Either
import Data.Maybe
import Data.Sequence (Seq(..), fromList, (|>), (<|))
import qualified Data.Sequence as S
import MyPrelude
import Data.List
import Control.Monad.Reader
import qualified Data.Map.Strict as M
import qualified Data.Set as St

main path = do
  input <- fromList . map (fromList . map (read @Int . (: []))) . lines <$> readFile path
  print $ partOne input
  print $ partTwo input

-- partOne :: Seq (Seq Int) -> Int
partOne heights = sum $ S.zipWith3 (\c (d,r) (u,l) -> if c < minimum [d,r,u,l]
                                                      then c + 1 else 0)
                                   (join heights) dr ul
  -- S.zip3 (join heights) dr ul
  where ds = join $ S.drop 1 heights |> fmap (const 10) (S.index heights 0)
        rs = heights >>= (S.drop 1 . (|> 10))
        ls = heights >>= (dropR 1 . (10 <|))
        us = join $ fmap (const 10) (S.index heights 0) <| heights

        dr = S.zip ds rs
        ul = S.zip us ls

dropR 0 s = s
dropR n (s :|> _) = dropR (n-1) s

-- partTwo :: Seq (Seq Int) -> Seq (Int, Int, Int)
partPoo heights = toList $ S.zipWith3 (\((c,col), row) (d,r) (u,l) ->
                 if c < minimum [d,r,u,l] then Right ((col, row), c)
                                          else Left  ((col, row), c))
                  cs dr ul
  where ds = join $ S.drop 1 heights |> fmap (const 10) (S.index heights 0)
        rs = heights >>= (S.drop 1 . (|> 10))
        ls = heights >>= (dropR 1 . (10 <|))
        us = heights >>= (10 <|)
        cs :: Seq ((Int, Int), Int)
        cs = join $ S.zipWith (\s i -> fmap (, i) s)
                    (heights <&> (`S.zip` S.fromList [0 .. length (S.index heights 0)]))
                    (S.fromList [0 .. length heights])

        dr = S.zip ds rs
        ul = S.zip us ls

-- partTwo :: Seq (Seq Int) -> Int
partTwo input =
  let (nonLowpoints, lowPoints) = partitionEithers $ partPoo input
      funny :: ((Int, Int), Int) -> Int
      funny l = length
                $ runReader (fixPointNewNei (St.singleton l) (St.singleton l))
                                  (M.fromList nonLowpoints)
  in
    product . take 3 . reverse . sort $
    map funny lowPoints

getNeighbour :: ((Int, Int), Int)
             -> Reader (M.Map (Int, Int) Int) [Maybe ((Int, Int), Int)]
getNeighbour ((cl, rw), vl) = forM [(0, 1), (0, -1), (1, 0), (-1, 0)] $
  \(x, y) -> asks (fmap ((cl + x, rw + y) , ) . M.lookup (cl + x, rw + y))

fixPointNewNei :: St.Set ((Int, Int), Int)
               -> St.Set ((Int, Int), Int)
               -> Reader (M.Map (Int, Int) Int) (St.Set ((Int, Int), Int))
fixPointNewNei total nei = do
  newNei <- St.filter ((/= 9) . snd) . St.fromList . catMaybes . concat <$>
            mapM getNeighbour (toList nei)
  let actualNewNei = newNei St.\\ total
  if St.null actualNewNei
    then return total
    else fixPointNewNei (actualNewNei `St.union` total) actualNewNei
