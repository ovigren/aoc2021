module D15.Chiton where
import MyPrelude
import qualified Data.Map.Strict as M
import Control.Monad
import Control.Applicative
import Control.Monad.RWS.Strict
import Data.Maybe
import Data.List
import Data.Function

type Values = M.Map (Int, Int) Int

main path = do
  input <- map (map $ read @Int . (:[])) . lines <$> readFile path

  print $ partOne input
  print $ partTwo input

partOne :: [[Int]] -> Int
partOne = shortestPath
          . M.fromList . concat
          . zipWith (\y -> zipWith (\x e -> ((x,y), e)) [0..]) [0..]
partTwo :: [[Int]] -> Int

partTwo = shortestPath
          . M.fromList . concat
          . zipWith (\y -> zipWith (\x e -> ((x,y), e)) [0..]) [0..]
          . map (map (\x -> if x > 9 then x - 9 else x)) -- wrap
          . concat . take 5 . iterate (map (map succ))   -- 5x height
          . map (concat . take 5 . iterate (map succ))   -- 5x length

shortestPath :: Values -> Int
shortestPath nodes = (\(_,s,_) -> fromJust $ M.lookup (xMax, yMax) s)
           $ runRWS (mapM_ step taxiCoords) nodes (M.singleton (0, 0) 0)
  where xMax = maximum (map fst (M.keys nodes))
        yMax = maximum (map snd (M.keys nodes))
        taxiCoords = tail . sortBy (compare @Int `on` uncurry (+))
                     $ [(x, y) | x <- [0 .. yMax] , y <- [0 .. xMax]]

askRisk :: (Int, Int) -> RWS Values () b Int
askRisk n = asks $ fromJust . M.lookup n

getMin :: (Int, Int) -> RWS a () Values (Maybe Int)
getMin n = gets $ M.lookup n

step :: (Int, Int) -> RWS Values () Values ()
step (x, y) = do
  nodeV <- askRisk (x, y)
  left <- getMin (x-1, y)
  up <- getMin (x, y-1)
  let localMin = nodeV + fromJust (min up left <|> up <|> left)
  modify $ M.insert (x, y) localMin

  propagate localMin (x-1, y)
  propagate localMin (x, y-1)


propagate :: Int -> (Int, Int) -> RWS Values () Values ()
propagate neighMin (x, y) =
  gets (M.lookup (x, y)) >>= \case
    Nothing -> return ()
    Just currentMin -> do
      newRouteMin <- (neighMin +) <$> asks (fromJust . M.lookup (x, y))
      when (newRouteMin < currentMin) $ do
        modify $ M.insert (x, y) newRouteMin
        mapM_ (propagate newRouteMin) [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
