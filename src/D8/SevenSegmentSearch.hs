module D8.SevenSegmentSearch where
import MyPrelude
import Control.Arrow
import Data.Maybe
import Data.List
import Data.Tuple (swap)

main path = do
  input <- map (dmap (map sort . words) . splitPairOn "|") . lines
           <$> readFile path
  print $ partOne input
  print $ partTwo input

partOne :: [([String], [String])] -> Int
partOne = length . concatMap (mapByLength . snd)

-- | Finds the segment to digit map for the digits uniquely determined by amount of segments.
mapByLength :: [String] -> [(String, Int)]
mapByLength = mapMaybe $
              traverse (flip lookup amountMap . length)
              . (id &&& id)
  where amountMap = [(2, 1), (3, 7), (4, 4), (7, 8)]

partTwo :: [([String], [String])] -> Int
partTwo = sum . map singleEntryOutput

-- | Find number of a single entry output
singleEntryOutput :: ([String], [String]) -> Int
singleEntryOutput (signal, output) =
  read @Int . concatMap show . findDigits $ output
  where
    findDigits :: [String] -> [Int]
    findDigits = map (fromJust . flip lookup segToDigit)

    segMap1478 :: [(String, Int)]
    segMap1478 = nub $ mapByLength $ output ++ signal

    -- Five segment digits
    len5 = nub $ filter ((5 ==) . length) (output ++ signal)
    seg3 = fromJust $ find ((== 2) . length . intersect cf) len5
    seg5 = fromJust $ find ((== 2) . length . intersect bd) len5
    seg2 = head (len5 \\ [seg3, seg5])

    -- Six segment digit
    len6 = nub $ filter ((6 ==) . length) (output ++ signal)
    seg6 = fromJust $ find ((== 1) . length . intersect cf) len6
    seg9 = fromJust $ find ((== 2) . length . intersect bd) (len6 \\ [seg6])
    seg0 = head (len6 \\ [seg6, seg9])

    segToDigit :: [(String, Int)]
    segToDigit = (seg2, 2) : (seg3, 3) : (seg5, 5) :
                 (seg6, 6) : (seg9, 9) : (seg0, 0) : segMap1478

    digToSeg :: [(Int, String)]
    digToSeg = map swap segMap1478

    -- Segment b and d
    bd = (\\) (fromJust (lookup 4 digToSeg))
              (fromJust (lookup 1 digToSeg))

    -- Segment c and f
    cf = intersect (fromJust (lookup 1 digToSeg))
                   (fromJust (lookup 7 digToSeg))


{-
   5 segments:           | 6 segments:
-------------------------|---------------------------
    2:      3:      5:   |   0:       6:      9:
   aaaa    aaaa    aaaa  |  aaaa     aaaa    aaaa
  .    c  .    c  b    . | b    c   b    .  b    c
  .    c  .    c  b    . | b    c   b    .  b    c
   dddd    dddd    dddd  |  ....     dddd    dddd
  e    .  .    f  .    f | e    f   e    f  .    f
  e    .  .    f  .    f | e    f   e    f  .    f
   gggg    gggg    gggg  |  gggg     gggg    gggg
-------------------------|--------------------------
  Uniquely determine     |
    2 -> has e, not f    |   0 -> not d
    3 -> has cf          |   6 -> not c
    5 -> has b, not c    |   9 -> not e
----------------------------------------------------

    1:       4:       7:      8:
   ....     ....     aaaa    aaaa
  .    c   b    c   .    c  b    c
  .    c   b    c   .    c  b    c
   ....     dddd     ....    dddd
  .    f   .    f   .    f  e    f
  .    f   .    f   .    f  e    f
   ....     ....     ....    gggg

  cf = 1 ∩ 7
  bd = 4 \ 1

  5 segments:
    3 = filter ((== 2) . length . intersect cf) 5chars
    5 = filter ((== 2) . length . intersect bf) 5chars
    2 = 5chars - 3 - 5

  6 segments:
    6 = filter ((== 1) . length . intersect cf) 6chars
    9 = filter ((== 2) . length . intersect bf) (6chars - 6)
    0 = 6chars - 0 - 9

-}
