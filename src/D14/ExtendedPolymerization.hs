module D14.ExtendedPolymerization where
import MyPrelude
import Control.Arrow
import qualified Data.Map as M
import Data.List

type Rules = M.Map String Char
-- | Amount of times consecutive element pair appears in polymer sequence.
type PairCount = [((Char, Char), Int)]

main path = do
  (seq, rules) <- second (M.fromList . map (second head . splitPairOn " -> ") . lines)
                  . splitPairOn "\n\n" <$> readFile path
  print $ partX 10 rules seq
  print $ partX 40 rules seq

partX :: Int -> Rules -> String -> Int
partX n rules seq = maximum elemOcc - minimum elemOcc
  where
    initPairC, finalPairC :: PairCount
    initPairC = map (head &&& length) . group . sort
                . zip seq $ tail seq
    finalPairC = applyN n (step rules) initPairC
    -- Times a given element X occurs in a sequence is
    -- the sum of the times pair XY occurs for all Y
    -- + 1 if X is last element of the whole sequence.
    elemOcc :: [Int]
    elemOcc = map snd . unionSum
              . ((last seq, 1) :)
              . map (first fst)
              $ finalPairC

step :: Rules -> PairCount -> PairCount
step rules pairs = unionSum $ pairs >>= \((l, r), n) ->
  case M.lookup [l, r] rules of
    -- No rule found, no element inserted
    Nothing -> [((l, r), n)]
    -- Element c inserted between all lr pairs. Creates n lc and n cr pairs.
    Just c -> [((l, c), n),  ((c, r), n)]

-- | For all pairs with equal first elements, sum all the second numbers.
unionSum :: (Ord a, Num b) => [(a, b)] -> [(a, b)]
unionSum = map (fst . head &&& sum . map snd)
              . groupBy (\a b -> fst a == fst b)
              . sortOn fst
