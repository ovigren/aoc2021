module D11.DumboOctopus where
import MyPrelude
import qualified Data.Map.Strict as M
import Control.Monad.State
import Data.List (groupBy, sortOn)
import Control.Arrow

main path = do
  input <- map (map (read @Int . (: []))) . lines <$> readFile path
  let start = M.fromList $ concat $ zipWith
              (\cs row -> zipWith
                (\c col -> ((col, row), c)) cs [0..]) input [0..]
  -- print . mapToList . fst $ runState (stepN 2 start) 0
  print $ fst $ execState (stepN 100 start) (0, 0)
  print $ snd $ execState (stepUntil (all (0 ==)) start) (0, 0)

mapToList :: M.Map (Int, Int) Int -> [[Int]]
mapToList = map (map snd) . groupBy (\a b -> snd (fst a) == snd (fst b))
        . sortOn (snd . fst)
        . M.toList

type Octopi = M.Map (Int, Int) Int

stepUntil bf a = if bf a then return a else step a >>= stepUntil bf

stepN 0 a = return a
stepN n a = step a >>= stepN (n-1)
step :: Octopi -> State (Int, Int) Octopi
step octopi = do
  let stepOne = M.map (+1) octopi
  stepTwo <- flashFixpoint stepOne stepOne
  let stepThree = M.map (\e -> if e > 9 then 0 else e) stepTwo
  modify (second (+1))
  return stepThree

flashFixpoint :: Octopi
              -> Octopi
              -> State (Int, Int) Octopi
flashFixpoint octopi hasntFlashed =
  let flasher = M.filter (9 <) hasntFlashed in
    if M.null flasher then return octopi
    else do
      modify (first (length flasher +))
      let newNei = foldl (flip $ M.adjust (+ 1))
                         octopi
                         [(x+dx, y+dy) | (x,y) <- M.keys flasher,
                          dx <- [-1,0,1], dy <- [-1,0,1] , (dx,dy) /= (0,0)
                         ]
      flashFixpoint newNei (M.intersection newNei $ hasntFlashed M.\\ flasher)
