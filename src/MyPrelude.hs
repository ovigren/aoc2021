module MyPrelude (splitOn, splitPairOn, bimap, dmap, applyN, parseDet
                 , (>->) , (<&>), (&), (>=>), (<=<)) where
import Data.Bifunctor
import Data.List.Split
import Data.Functor
import Data.Function
import Control.Monad
import Text.ParserCombinators.ReadP (readP_to_S, ReadP)

splitPairOn :: Eq a => [a] -> [a] -> ([a], [a])
splitPairOn split str = case splitOn split str of
  [a, b] -> (a, b)

dmap :: Bifunctor p => (a -> b) -> p a a -> p b b
dmap f = bimap f f

infixl 9 >->
(>->) :: (a -> b) -> (b -> c) -> (a -> c)
f >-> g = g . f

applyN 0 f = id
applyN n f = applyN (n-1) f . f

-- | Parse exactly one no remaining input way.
-- Run-time error with all parse options and remaining input otherwise.
parseDet :: Show a => ReadP a -> String -> a
parseDet p i = let parsed = readP_to_S p i in
  case filter ((== "") . snd) parsed of
    []       -> error $ "No parse!\n  " ++ show parsed
    [(p, _)] -> p
    _        -> error $ "Non-deterministic parse!\n  " ++ show parsed
