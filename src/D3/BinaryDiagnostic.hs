module D3.BinaryDiagnostic where

import MyPrelude
import Data.List
import Data.Digits
import Control.Arrow
import Numeric

main path = do
  input <- map (map (read @Int . (: [])))
         . lines
         <$> readFile path
  print $ partOne $ input
  print $ partTwo $ input


partOne :: [[Int]] -> Int
partOne = uncurry (*)
          . dmap (unDigits 2 . map fromEnum)
          . (id &&& map not)
          . map (uncurry (>) . dmap length . break (/= 0) . sort)
          . transpose

partTwo :: [[Int]] -> Int
partTwo input = oxy * co2
  where
    oxy = unDigits 2 $ filt' input 1

    co2 = unDigits 2 $ filt' input 0

    mode :: [Int] -> Ordering
    mode = uncurry compare . dmap length . break (/= 0) . sort

    filt' = filt 0
    filt i [] _ = error "fan"
    filt i [n] _ = n
    filt i ns isOxy = filt (i+1) (filter (\n -> m (n !! i)) ns) isOxy
      where m = ma (mode $ head $ drop i $ transpose ns)
            ma :: Ordering -> Int -> Bool
            ma EQ b = isOxy == b -- Likamånga
            ma LT b = isOxy == b -- Most ones
            ma GT b = isOxy /= b -- Most zeroes
