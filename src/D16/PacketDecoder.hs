module D16.PacketDecoder where
import MyPrelude (parseDet)
import Data.Digits (unDigits)
import Text.ParserCombinators.ReadP (ReadP, char, get, count, many1, string, many, (+++), (<++), eof)

main path = do
  input <- concatMap hexToBin . head . lines <$> readFile path :: IO String
  let parseTree = parseDet readTopLevelPacket input
  print $ versionSum parseTree
  print $ eval parseTree

type Packet = (Int, PacketType)
data PacketType = Literal Int -- id 4
                | Operator OpType [Packet]
                deriving (Show)

data OpType = Sum | Prod | Min | Max | AlignEnum | Gt | Lt | Equ
            deriving (Show, Enum)

versionSum :: Packet -> Int
versionSum (i, Literal _) = i
versionSum (i, Operator _ pkgs) = i + sum (map versionSum pkgs)

eval :: Packet -> Int
eval (_, Literal n) = n
eval (_, Operator t pkgs) = evalOp t (map eval pkgs)
  where evalOp = \case
          Sum  -> sum
          Prod -> product
          Min  -> minimum
          Max  -> maximum
          Gt   -> \case [a, b] -> if a > b  then 1 else 0
          Lt   -> \case [a, b] -> if a < b  then 1 else 0
          Equ  -> \case [a, b] -> if a == b then 1 else 0

readTopLevelPacket :: ReadP Packet
readTopLevelPacket = readPacket <* many (char '0') <* eof

readPacket :: ReadP Packet
readPacket = do
  version <- readBinToDec <$> count 3 get
  packetType <- readLiteral <++ readOperator
  return (version, packetType)

readLiteral :: ReadP PacketType
readLiteral = do
  string "100"
  firstbits <- many $ char '1' >> count 4 get
  finalbits <- char '0' >> count 4 get
  return $ Literal $ readBinToDec $ concat $ firstbits ++ [ finalbits ]

readOperator :: ReadP PacketType
readOperator = do
  opType <- toEnum . readBinToDec <$> count 3 get
  subPackets <- readBitLenType +++ readPkgNumType
  return $ Operator opType subPackets
  where
    readBitLenType :: ReadP [Packet]
    readBitLenType = do
      char '0'
      numOfBits <- readBinToDec <$> count 15 get
      stringToParse <- count numOfBits get
      return $ parseDet (many1 readPacket) stringToParse

    readPkgNumType :: ReadP [Packet]
    readPkgNumType = do
      char '1'
      numOfPkgs <- readBinToDec <$> count 11 get
      count numOfPkgs readPacket

readBinToDec :: String -> Int
readBinToDec = unDigits 2 . map (read @Int . (: []))

hexToBin :: Char -> String
hexToBin = \case
  '0' -> "0000"
  '1' -> "0001"
  '2' -> "0010"
  '3' -> "0011"
  '4' -> "0100"
  '5' -> "0101"
  '6' -> "0110"
  '7' -> "0111"
  '8' -> "1000"
  '9' -> "1001"
  'A' -> "1010"
  'B' -> "1011"
  'C' -> "1100"
  'D' -> "1101"
  'E' -> "1110"
  'F' -> "1111"
  _ -> error "not a dec digit"
