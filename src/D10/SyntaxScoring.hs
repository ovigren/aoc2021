module D10.SyntaxScoring where
import MyPrelude
import Text.ParserCombinators.ReadP
import Data.Tree
import Data.Either
import Data.List

main path = do
  input <- lines <$> readFile path
  let parsed = map (readP_to_S parseLine) input
  print . sum . map points . lefts . map fst $ concatMap (take 1) parsed
  print . (\s -> s !! (length s `div` 2)) . sort
        . map (foldl points2 0 . reverse . flatten)
        . rights . map fst
        $ concatMap (take 1) parsed
  return ()

closing = ")]}>"
opening = "([{<"

points = \case ')' -> 3 ; ']' -> 57 ; '}' -> 1197 ; '>' -> 25137

points2 i (_, Just _) = i
points2 i (open, Nothing) = i * 5 +
  case open of '(' -> 1 ; '[' -> 2 ; '{' -> 3 ; '<' -> 4

opposite = \case
  '(' -> ')' ; ')' -> '('
  '[' -> ']' ; ']' -> '['
  '{' -> '}' ; '}' -> '{'
  '<' -> '>' ; '>' -> '<'

-- | Non-matching closing parenthesis.
type BadClose = Char
-- | Opening paren and maybe closing parenthesis, else need autocomplete.
type ParenTree = Tree (Char, Maybe Char)

parseLine, parseChunk :: ReadP (Either BadClose ParenTree)
parseLine = parseChunk >>= \case
  a@(Left _) -> return a         -- Bad closer can terminate early.
  b@(Right _) -> eof >> return b -- Good closer / uncompleted may not
parseChunk = do
  -- An opening parenthesis
  open <- choice $ map char opening

  -- Either losing character of bad chunks or good chunks
  mChunks <- sequence <$> many parseChunk

  -- If there's a bad chunk we terminate early.
  case mChunks of
    Left bad -> return $ Left bad
    Right goodChunks -> do
      close <- fmap Right (fmap Just (char $ opposite open) -- Good closer
                          <++ (eof >> return Nothing))      -- Needs to be completed
               <++ fmap Left (choice $ map char closing)    -- Bad closer

      return $ close <&> \b -> Node { rootLabel = (open, b)
                                    , subForest = goodChunks
                                    }
