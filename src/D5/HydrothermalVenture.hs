module D5.HydrothermalVenture where
import MyPrelude (dmap, splitPairOn)
import Data.List

main path = do
  input <- map (dmap (dmap (read @Int) . splitPairOn ",") . splitPairOn " -> ")
           . lines <$> readFile path

  print $ twoOrMore $ concatMap lineToPoints input
  print $ twoOrMore $ concatMap lineToPoints2 input

twoOrMore :: [(Int, Int)] -> Int
twoOrMore = length . filter (>= 2) . map length . group . sort

lineToPoints :: ((Int, Int), (Int, Int)) -> [(Int, Int)]
lineToPoints ((a,b) , (c,d)) | a == c = -- Vertical
                               (a, ) <$> if b < d then [b .. d] else [d .. b]
lineToPoints ((a,b) , (c,d)) | b == d = -- Horizontal
                               (, b) <$> if a < c then [a .. c] else [c .. a]
                             | otherwise = [] -- Diagonal

lineToPoints2 :: ((Int, Int), (Int, Int)) -> [(Int, Int)]
lineToPoints2 ((a,b) , (c,d)) = let dx = signum $ c - a
                                    dy = signum $ d - b
                                in zip [a, a + dx .. c] [b, b + dy .. d]
