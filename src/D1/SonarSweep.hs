module D1.SonarSweep where
main path = do
  depths <- map (read @Int) <$> lines <$> readFile path
  putStrLn $ show $ partOne depths
  putStrLn $ show $ partTwo depths

partOne x = length $ filter (== True) $ zipWith (>) (drop 1 x) x

partTwo x = length $ filter (== True) $ zipWith (>) (drop 1 w) w
  where
    w = zipWith (+) (drop 2 x) $ zipWith (+) (drop 1 x) x


testData = [ 199 , 200 , 208 , 210 , 200 , 207 , 240 , 269 , 260 , 263 ]
