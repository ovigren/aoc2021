module D6.Lanternfish where
import MyPrelude
import Data.Sequence hiding (length , filter)

main path = do
  input <- map (read @Int) . splitOn "," <$> readFile path
  print $ partOne input
  print $ partTwo input

partOne, partTwo :: [Int] -> Int
partOne = sum . (!! 80) . timeline
partTwo = sum . (!! 256) . timeline

timeline :: [Int] -> [Seq Int]
timeline start = iterate tick . fromList $
                 [ length $ filter (n ==) start | n <- [0..8]]
  where
    tick :: Seq Int -> Seq Int
    tick (n :<| ns) = adjust' (n +) 6  $ ns |> n
